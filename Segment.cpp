#include "Segment.h"

Segment::Segment(Point p1, Point p2) : p1(p1), p2(p2){}

Segment::~Segment()
{
}

Point Segment::getVect() {
    float x = p2.x - p1.x;
    float y = p2.y - p1.y;
    float z = p2.z - p1.z;
    return Point(x,y,z);
}