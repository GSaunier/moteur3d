#ifndef POINT_H
#define POINT_H

#include <ctgmath>
#include <iostream>
#include <cstdlib>
using namespace std;



class Point
{
    public:
        float x,y,z;
        Point(float x, float y, float z);
        Point(float x, float y);
        virtual ~Point();
        Point vectorielle(Point p);
        float scalaire(Point p);
        float norme();
        void normalize();
        Point getVec(Point p);
        Point baricentre(Point p1, Point p2);
        void arrond();
        Point operator-(const Point p1);
        float operator*(const Point p1);
        float get(int coord);
        void set(int coord, float v);
        void afficher();

    protected:

    private:
};



#endif // POINT_H

