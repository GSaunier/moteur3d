#include "Matrice.h"

void Matrice::identity() {
    int i = 0;
    for(int j = 0; j < 4; j++) {
        col[j].ligne[i] = 1;
        i++;
    }
}

void Matrice::projection(float distance) {
    this->identity();
    this->col[2].ligne[3] = -1.f/distance;
}

void Matrice::afficher() {

    for(int i = 0; i < 4; i++) {
        cout << "|" << ends;
        for(int j = 0; j < 4; j++) {
            cout << col[j].ligne[i] << "|" << ends;
        }
        cout << "" << endl;
    }
    cout << "" << endl;
}

void Matrice::viewPort(int x, int y, int w, int h,int d) {
    this->identity();
    col[3].ligne[0] = x + w/2.f;
    col[3].ligne[1] = y + h/2.f;
    col[3].ligne[2] = d/2.f;

    col[0].ligne[0] = w/2.f;
    col[1].ligne[1] = h/2.f;
    col[2].ligne[2] = d/2.f;
}

Point Matrice::matToVec() {
    float coef = get(0,3);
    return Point(get(0,0)/coef,get(0,1)/coef,get(0,2)/coef);
}

Matrice Matrice::vecToMat(const Point &p) {
    Matrice ret = Matrice();
    ret.set(0,0,p.x);
    ret.set(0,1,p.y);
    ret.set(0,2,p.z);
    ret.set(0,3,1.f);

    return ret;
}

Matrice Matrice::vecToMat(const Point &p, float v) {
    Matrice ret = Matrice();
    ret.set(0,0,p.x);
    ret.set(0,1,p.y);
    ret.set(0,2,p.z);
    ret.set(0,3,v);

    return ret;
}

Point Matrice::toCoordonate(Matrice vp, Matrice mv, const Point& point,Matrice &ndc_tri,int index) {
    Matrice m = vp *(*this) * mv;
    Matrice n = (*this) * mv;
    Matrice m1 = vecToMat(point);
    //m1.afficher();
    Matrice f = Matrice();
    Matrice g = Matrice();

    for(int l = 0; l < 4; l++) {
        float v = 0;
        float w = 0;
        for(int c = 0; c < 4; c++) {
            v += m.get(c,l) * m1.get(0,c);
            w += n.get(c,l) * m1.get(0,c);
        }
        f.set(0,l,v);
        g.set(0,l,w);
    }
    Point p = g.matToVec();
    for (int i = 0; i < 3; ++i) {
        ndc_tri.set(index,i,p.get(i));
    }


    return f.matToVec();

}

Matrice Matrice::operator*(Matrice m) {
    Matrice ret = Matrice();
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j <4; j++) {
            float v = 0;
            for(int k = 0; k < 4; k++) {
                v+= get(k,j)*m.get(i,k);
            }
            ret.set(i,j,v);
        }
    }
    return ret;

}

float Matrice::get(int c, int l) {
    return col[c].ligne[l];
}

void Matrice::set(int c, int l, float v) {
    col[c].ligne[l] = v;
}

Matrice Matrice::invert_transpose3() {
    Matrice m = adjugate3();
    float v = 0;
    for (int i = 0; i < 3; ++i) {
        v+= m.get(i,0)*get(i,0);
    }

    for (int j = 0; j < 3; ++j) {
        for (int i = 0; i < 3; ++i) {
            m.set(i,j,m.get(i,j)/v);
        }
    }

    return m;
}


Matrice Matrice::invert_transpose() {
    Matrice m = adjugate();
    float v = 0;
    for (int i = 0; i < 4; ++i) {
        v+= m.get(i,0)*get(i,0);
    }

    for (int j = 0; j < 4; ++j) {
        for (int i = 0; i < 4; ++i) {
            m.set(i,j,m.get(i,j)/v);
        }
    }

    return m;
}

Point Matrice::getProj(Matrice simple) {
    Matrice f = Matrice();

    for(int l = 0; l < 4; l++) {
        float v = 0;
        for(int c = 0; c < 4; c++) {
            v += get(c,l) * simple.get(0,c);
        }
        f.set(0,l,v);
    }
    Point ret = Point(f.get(0,0),f.get(0,1),f.get(0,2));
    ret.normalize();
    return ret;
}

Matrice Matrice::adjugate() {
    Matrice ret = Matrice();
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            ret.set(j,i,cofactor(i,j,4,4));
        }
    }
    return ret;
}

Matrice Matrice::adjugate3() {
    Matrice ret = Matrice();
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            ret.set(j,i,cofactor(i,j,3,3));
        }
    }
    return ret;
}

float Matrice::cofactor(int l, int c, int row, int col) {
    float ret = get_minor(l,c,row,col).det(row-1,l,c)*((l+c)%2 ? -1 : 1);
    return ret;
}

Matrice Matrice::get_minor(int l, int c, int row, int col) {
    Matrice ret = Matrice();
    for (int i = 0; i < row-1; ++i) {
        for (int j = 0; j < col-1; ++j) {
            ret.set(j,i,get(j<c?j:j+1,i<l?i:i+1));
        }
    }
    return ret;
}

float Matrice::det(int row,int l,int c) {
    float ret = 0;
    if (row == 3){

        ret += get(0,0)*(get(1,1)*get(2,2) - get(2,1) * get(1,2));//-3

        ret -= get(1,0)*(get(0,1)*get(2,2)-get(2,1)*get(0,2));//-1

        ret += get(2,0)*(get(0,1)*get(1,2)-get(1,1)*get(0,2));//

        return ret;
    } else {
        ret = get(0,0)*get(1,1) - get(1,0)*get(0,1);
        return ret;
    }
}

void Matrice::transpose() {
    for (int i = 0; i < 4; ++i) {
        for (int j = i+1; j < 4; ++j) {
            float temp = get(i,j);
            set(i,j,get(j,i));
            set(j,i,temp);
        }
    }
}






