#include "Point.h"

Point::Point(float x, float y, float z) : x(x), y(y), z(z)
{
    //ctor
}

Point::Point(float x, float y) : x(x), y(y), z(0)
{
    //ctor
}

Point::~Point()
{
    //dtor
}

Point Point::vectorielle(Point p) {
    float x1 = y * p.z - z * p.y;
    float y1 = z * p.x - x * p.z;
    float z1 = x * p.y - y * p.x;
    return Point(x1,y1,z1);
}

float Point::scalaire(Point p) {
    //cout << z * p.z << endl;
    return x*p.x + y*p.y + z*p.z;
}

float Point::norme() {
    //cout << "x: " << x << " y: " << y << " z: " << z << endl;
    return sqrt(x*x+y*y+z*z);
}

void Point::arrond() {
    x = (int)x;
    y = (int)y;
    z = (int)z;
}

void Point::normalize() {
    //  cout << norme() << endl;
    //cout << x <<endl;
    float n = norme();
    x = x/n;
    y = y/n;
    z = z/n;
    //cout << x << endl;
}

Point Point::getVec(Point p) {
    float x1 = p.x - x;
    float y1 = p.y - y;
    float z1 = p.z - z;
    return Point(x1,y1,z1);
}

Point Point::baricentre(Point p1, Point p2) {
    float x1 = (x + p1.x + p2.x)/3;
    float y1 = (y + p1.y + p2.y)/3;
    float z1 = (z + p1.z + p2.z)/3;
    return Point(x1,y1,z1);
}

Point Point::operator-(const Point p1) {
    float x1 = x - p1.x;
    float y1 = y - p1.y;
    float z1 = z - p1.z;

    return  Point(x1,y1,z1);
}

float Point::get(int coord) {
    switch (coord) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
    }
    return 0;
}

void Point::set(int coord, float v) {
    switch (coord) {
        case 0:
            x = v;
            break;
        case 1:
            y = v;
            break;
        case 2:
            z = v;
            break;
    }

}

float Point::operator*(const Point p1) {
    float x1 = x * p1.x;
    float y1 = y * p1.y;
    float z1 = z * p1.z;

    return  x1+y1+z1;
}

void Point::afficher() {
    cout << "x: " << x << " y: " << y << " z: " << z << endl;
}



