#ifndef SEGMENT_H
#define SEGMENT_H
#include "Point.h"


class Segment
{
    public:
        Point p1;
        Point p2;
        Segment(Point p1, Point p2);
        virtual ~Segment();
        Point getVect();

    protected:

    private:
};

#endif // SEGMENT_H
