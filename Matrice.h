//
// Created by Asus ROH on 06/02/2019.
//

#ifndef M3D_MATRICE_H
#define M3D_MATRICE_H


#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include "Point.h"

using namespace std;
class Matrice {
    class Compo {
        public:
            float ligne[4];
    };
public:
    Compo col[4];
    void identity();
    void projection(float distance);
    void afficher();
    void viewPort(int x, int y, int w ,int h, int d);
    Point matToVec();
    Matrice vecToMat(const Point& p);

    Point toCoordonate(Matrice vp,Matrice mv,const Point& point,Matrice &ndc_tri,int index);
    Matrice operator*(Matrice m);
    float get(int c,int l);
    void set(int c,int l,float v);
    Matrice invert_transpose();
    Matrice invert_transpose3();
    Point getProj(Matrice simple);
    Matrice adjugate();
    Matrice adjugate3();
    float cofactor(int l, int c,  int row, int col);
    Matrice get_minor(int l, int c, int row, int col);
    float det(int row,int l,int c);
    void transpose();
    Matrice invert(Matrice m);
    Matrice vecToMat(const Point &p, float v);
};



#endif //M3D_MATRICE_H
