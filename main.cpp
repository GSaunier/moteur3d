// Moteur3D.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include "tgaimage.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "Point.h"
#include "Segment.h"
#include "Matrice.h"

using namespace std;

const TGAColor white = TGAColor(255, 255, 255, 255);
const TGAColor red = TGAColor(255, 0, 0, 255);
const TGAColor green = TGAColor(0, 255, 0, 255);
const TGAColor blue = TGAColor(0, 0, 255, 255);
const TGAColor black = TGAColor(0, 0, 0, 255);
const TGAColor screen = TGAColor(255, 0, 255, 255);

const int width  = 800;
const int height = 800;
const int depth = 255;

Point camera = Point(0,0,3);
Point centre = Point(0,0,0);
Point up = Point(0,1,0);
Point lumiere = Point(1,1,1);
Point variationLumiere = Point(0,0,0);

Matrice modelView = Matrice();
Matrice uni_M = Matrice();
Matrice uni_MIT = Matrice();
Matrice varNorm = Matrice();
Matrice ndcTri = Matrice();

TGAImage normalMap;
TGAImage specMap;
TGAImage glowMap;

void lookat() {
    Point z = (camera-centre);
    z.normalize();
    Point x = up.vectorielle(z);
    x.normalize();
    Point y = z.vectorielle(x);
    y.normalize();

    Matrice Minv = Matrice();
    Matrice Tr = Matrice();

    Minv.identity();
    Tr.identity();

    for(int i = 0; i < 3; i++){
        Minv.set(i,0,x.get(i));
        Minv.set(i,1,y.get(i));
        Minv.set(i,2,z.get(i));

        Tr.set(3,i, -centre.get(i));
    }

    modelView = Minv*Tr;
}

Point barycentric(Point *pts, Point p) {
    Point b0 = Point(0,0,0);
    b0.x = pts[2].x - pts[0].x;
    b0.y = pts[1].x - pts[0].x;
    b0.z = pts[0].x - p.x;

    Point b1 = Point(0,0,0);
    b1.x = pts[2].y - pts[0].y;
    b1.y = pts[1].y - pts[0].y;
    b1.z = pts[0].y - p.y;

    b0 = b0.vectorielle(b1);

    if(abs(b0.z) < 1)
        return Point(-1,1,1);

    return Point(1.f-(b0.x+b0.y)/b0.z,b0.y/b0.z,b0.x/b0.z);
}

Point getVariation2(Point uvf) {
    Point uv = Point(uvf.get(0)*normalMap.get_width(),uvf.get(1)*normalMap.get_height());
    //Récupération de la couleur sur la normal
    TGAColor c = normalMap.get(uv.get(0),uv.get(1));
    Point p = Point(0,0,0);
    for(int i = 0; i < 3 ; i++) {
        p.set(2-i,(float)c[i]/255.f*2.f - 1.f);
    }

    return p;
}

void triangle(Point *pts,Point *tex, float *zbuffer, int taille, TGAImage &image,TGAImage &texture) {
    int minx = taille-1;
    int miny = taille-1;
    int maxx = 0;
    int maxy = 0;

    for(int i = 0; i < 3; i++) {

        minx = std::fmax(0,fmin(minx,pts[i].x));
        miny = std::fmax(0,fmin(miny,pts[i].y));
        maxx = fmin(taille-1,fmax(maxx,pts[i].x));
        maxy = fmin(taille-1,fmax(maxy,pts[i].y));
    }

    Point p = Point(0,0,0);
    //cout << "minx: " << minx << " miny: " << miny << " maxx: " << maxx << " maxy: " << maxy << endl;
    for( p.x = minx; p.x <= maxx; p.x++) {
        for (p.y = miny; p.y <= maxy; p.y++) {
            Point bc = barycentric(pts,p);

            Point Bn = Point(0,0,0);

            for (int j = 0; j < 3; ++j) {
                float v = 0;
                for (int i = 0; i < 3; ++i) {
                    v += varNorm.get(i,j)*bc.get(i);
                }
                Bn.set(j,v);
            }
            Bn.normalize();

            float xTex = tex[0].x*bc.x +  tex[1].x*bc.y + tex[2].x*bc.z;
            float yTex = tex[0].y*bc.x +  tex[1].y*bc.y + tex[2].y*bc.z;
            Point pTex = Point(xTex,yTex);

            Matrice A = Matrice();
            Point p1 = Point(ndcTri.get(1,0),ndcTri.get(1,1),ndcTri.get(1,2));
            Point p0 = Point(ndcTri.get(0,0),ndcTri.get(0,1),ndcTri.get(0,2));
            Point p2 = Point(ndcTri.get(2,0),ndcTri.get(2,1),ndcTri.get(2,2));
            Point a0 = p1 - p0;
            Point a1 = p2 - p0;
            for (int i = 0; i < 3; ++i) {
                A.set(i,0,a0.get(i));
                A.set(i,1,a1.get(i));
                A.set(i,2,Bn.get(i));
            }

            Matrice AI = A.invert_transpose3();
            AI.transpose();
            Point aix = Point(tex[1].x - tex[0].x,tex[2].x - tex[0].x, 0);
            Point aiy = Point(tex[1].y - tex[0].y,tex[2].y - tex[0].y, 0);

            Point ai = Point(0,0,0);
            Point aj = Point(0,0,0);

            for (int k = 0; k < 3; ++k) {
                float vi = 0;
                float vj =0;
                for (int i = 0; i < 3; ++i) {
                    vi += AI.get(i,k)*aix.get(i);
                    vj += AI.get(i,k)*aiy.get(i);
                }
                ai.set(k,vi);
                aj.set(k,vj);
            }
            ai.normalize();
            aj.normalize();

            Matrice B = Matrice();

            for (int l = 0; l < 3; ++l) {
                B.set(0,l,ai.get(l));
                B.set(1,l,aj.get(l));
                B.set(2,l,Bn.get(l));
            }


            Point vn = getVariation2(pTex);
            Point specN = uni_MIT.getProj(B.vecToMat(vn,0));
            specN.normalize();



            Point n = Point(0,0,0);
            for (int k = 0; k < 3; ++k) {
                float vk = 0;
                for (int i = 0; i < 3; ++i) {
                    vk += B.get(i,k)*vn.get(i);
                }
                n.set(k,vk);
            }

            n.normalize();

            Point r = Point(n.get(0)*(n*lumiere)*2.f,n.get(1)*(n*lumiere)*2.f, n.get(2)*(n*lumiere)*2.f)-lumiere;
            float spec = pow(std::max(r.z,0.f),5+specMap.get(pTex.x * specMap.get_width(), pTex.y * specMap.get_height())[0]/1.f);
            float diffuse = max(0.f, n*lumiere);

            TGAColor c = texture.get(pTex.x * texture.get_width(), pTex.y * texture.get_height());
            for (int i=0; i<3; i++) c[i] = std::min<float>((c[i])*(diffuse + 1.2*spec ), 255);


            if(bc.x < 0 || bc.y < 0 || bc.z < 0)
                continue;
            p.z = 0;
            p.z += pts[0].z * bc.x;
            p.z += pts[1].z * bc.y;
            p.z += pts[2].z * bc.z;


            if(zbuffer[int(p.x+p.y*taille)] < p.z) {
                image.set(p.x,p.y,c);
                zbuffer[int(p.x + p.y * taille)] = p.z;
            }

        }
    }
}

void drawFile(string name, int taille, TGAImage &image, TGAImage &te, float *zbuffer) {
    ifstream fichier(name.c_str(), ios::in);  // on ouvre le fichier en lecture
    if (fichier) {
        string ligne;
        float dim = ((float)taille / 2);

        vector<Point> points;
        vector<Point> norme;
        vector<Point> texture;
        Matrice m = Matrice();
        Matrice vp = Matrice();
        m.projection((camera-centre).norme());
        vp.viewPort(width/8, height/8, width*3/4, height*3/4,depth);
        lookat();
        uni_M = m*modelView;
        uni_MIT = uni_M.invert_transpose();
        Matrice light = Matrice();
        light.set(0,0,lumiere.get(0));
        light.set(0,1,lumiere.get(1));
        light.set(0,2,lumiere.get(2));
        light.set(0,3,0);

        lumiere = uni_M.getProj(light);
        lumiere.normalize();

        while (getline(fichier, ligne)) {
            stringstream source(ligne);
            string option;
            source >> option;
            if (option == "v") {
                float x;
                float y;
                float z;
                source >> x >> y >> z;
                //cout << "x: " << x << " y: " << y << " z: " << z << endl;
                Point v = Point(x,y,z);

                points.push_back(v);

                //point(x,y,image,white);
            } else if (option == "vt") {
                float x;
                float y;
                source >> x >> y;
                texture.push_back(Point(x,y));
            } else if (option == "vn") {
                float x,y,z;
                source >> x >> y >> z;
                norme.push_back(Point(x,y,z));
            }
            else if (option == "f") {
                int cp1, tex1, cp2,tex2,cp3,tex3,nor1,nor2,nor3;
                string trash;
                char slashtrash;

                source >> cp1 >> slashtrash >> tex1 >> slashtrash >> nor1;
                source >> cp2 >> slashtrash >> tex2 >> slashtrash >> nor2;
                source >> cp3 >> slashtrash >> tex3 >> slashtrash >> nor3;
                //cout << "x: " << vN.x << " y: " << vN.y << " z: " << vN.z << endl;
                // Calcul scalaire de deux vecteur.

                Point pts[3] = {points[cp1 - 1], points[cp2 - 1],points[cp3 - 1]};
                Point tex[3] = {texture[tex1-1], texture[tex2-1], texture[tex3-1]};
                Point nor[3] = {norme[nor1-1], norme[nor2-1], norme[nor3-1]};

                for (int i = 0; i < 3; ++i) {
                    Point p = m.toCoordonate(vp,modelView,pts[i],ndcTri,i);
                    pts[i] = p;
                }

                //ndcTri.afficher();

                for (int i = 0; i < 3; ++i) {
                    Point norma = nor[i];
                    norma.normalize();
                    Matrice mat = Matrice();
                    mat.set(0,0,norma.get(0));
                    mat.set(0,1,norma.get(1));
                    mat.set(0,2,norma.get(2));
                    mat.set(0,3,0);
                    norma = uni_MIT.getProj(mat);
                    for (int j = 0; j < 3; ++j) {
                        varNorm.set(i,j,norma.get(j));
                    }
                }


                triangle(pts,tex,zbuffer,taille, image,te );
            }
        }
        fichier.close();  // on ferme le fichier
    }
    else  // sinon
        cerr << "Impossible d'ouvrir le fichier !" << endl;
}

void loadTexture(string path, TGAImage &texture) {
    texture.read_tga_file(path.c_str());
    texture.flip_vertically();
}

void flouImage(TGAImage &glow, int intensite) {
    TGAImage flou(width,width, TGAImage::RGB);
    for (int k = 0; k < intensite; ++k) {


        for (int i = 0; i < glow.get_width(); ++i) {
            for (int j = 0; j < glow.get_height(); ++j) {
                float moyenneB = 0;
                float moyenneG = 0;
                float moyenneR = 0;
                int nbPixel = 0;
                moyenneB += glow.get(i, j)[0];
                moyenneG += glow.get(i, j)[1];
                moyenneR += glow.get(i, j)[2];
                nbPixel++;
                if (i != 0) {
                    moyenneB += glow.get(i - 1, j)[0];
                    moyenneG += glow.get(i - 1, j)[1];
                    moyenneR += glow.get(i - 1, j)[2];
                    nbPixel++;
                    if (j != 0) {
                        moyenneB += glow.get(i - 1, j - 1)[0];
                        moyenneG += glow.get(i - 1, j - 1)[1];
                        moyenneR += glow.get(i - 1, j - 1)[2];
                        nbPixel++;
                    }
                    if (j != glow.get_height() - 1) {
                        moyenneB += glow.get(i - 1, j + 1)[0];
                        moyenneG += glow.get(i - 1, j + 1)[1];
                        moyenneR += glow.get(i - 1, j + 1)[2];
                        nbPixel++;
                    }
                }
                if (i != glow.get_width() - 1) {
                    moyenneB += glow.get(i + 1, j)[0];
                    moyenneG += glow.get(i + 1, j)[1];
                    moyenneR += glow.get(i + 1, j)[2];
                    nbPixel++;
                    if (j != 0) {
                        moyenneB += glow.get(i + 1, j - 1)[0];
                        moyenneG += glow.get(i + 1, j - 1)[1];
                        moyenneR += glow.get(i + 1, j - 1)[2];
                        nbPixel++;
                    }
                    if (j != glow.get_height() - 1) {
                        moyenneB += glow.get(i + 1, j + 1)[0];
                        moyenneG += glow.get(i + 1, j + 1)[1];
                        moyenneR += glow.get(i + 1, j + 1)[2];
                        nbPixel++;
                    }
                }
                moyenneB = moyenneB/(float)nbPixel;
                moyenneG = moyenneG/(float)nbPixel;
                moyenneR = moyenneR/(float)nbPixel;
                flou.set(i, j, TGAColor(moyenneR, moyenneG, moyenneB,255));
            }

        }
        glow = flou;
    }
}

void drawGlow(TGAImage &image, TGAImage &glow, TGAImage &base) {
    for (int i = 0; i < image.get_width(); ++i) {
        for (int j = 0; j < image.get_height(); ++j) {
            TGAColor baseC = base.get(i,j);
            TGAColor glowC = glow.get(i,j);
            for (int k = 0; k < 3; ++k) {
                if(glowC[k] != 0)
                baseC[k] = min((5.f+ baseC[k])*(glowC[k]),255.f);
                else
                    baseC[k]+=5.f;
            }
            image.set(i,j,baseC);
        }
    }
}

void draw(string path, int taille, TGAImage& image, float *zbuffer) {
    TGAImage texture;
    TGAImage glowIm = TGAImage(taille, taille, TGAImage::RGB);
    TGAImage base = TGAImage(taille, taille, TGAImage::RGB);
    loadTexture("../Ressource/"+ path +"_diffuse.tga", texture);
    loadTexture("../Ressource/"+path+"_nm_tangent.tga", normalMap);
    loadTexture("../Ressource/"+path+"_spec.tga", specMap);
    loadTexture("../Ressource/"+path+"_glow.tga", glowMap);

    drawFile("../Ressource/"+ path + ".obj", taille, base, texture,zbuffer);

    vector<float> zbufGlow(taille*taille,1e-2);

    drawFile("../Ressource/"+ path + ".obj", taille, glowIm, glowMap,zbufGlow.data());

    flouImage(glowIm,3);

    drawGlow(image,glowIm,base);


}

const TGAColor darkRed = TGAColor(40, 10, 10, 255);

void setBackground(TGAImage &image, TGAColor c) {
    for (int i = 0; i < image.get_width(); ++i) {
        for (int j = 0; j < image.get_height(); ++j) {
            image.set(i,j,c);
        }
    }
}

int main(int argc, char** argv) {
    int taille = width;

    TGAImage image = TGAImage(taille, taille, TGAImage::RGB);

    //setBackground(image,darkRed);

    vector<float> zbuffer(taille*taille,1e-2);

    string path = "head";


    path = "diablo3_pose";
    draw(path,taille,image,zbuffer.data());


    image.flip_vertically();
    image.write_tga_file("../Ressource/Rendu/renduFinal.tga");

    return 0;
}
